#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Script for generating traffic to flackr
"""
import urllib2
import urllib
import random
import os
import json
import getopt
import sys

"""Options:
 U = URL REQUIRED OPTION
 P = add new picture
 V = views + A all or R random(writes webpagetotal x2 random views)
 F = download "visit" frontPage
 C = comment + A all, or R random(gen number of comments(max 1/2 total)
                             and then comments to random id)
 H = adds H_RANGE pictures, visits frontpage, comments to all
        visits all pictures once
 R = Either inserts picture, comments with opt R, views with opt R
      or visits frontpage F

 Example: ./trafficScript.py -U "http://192.168.200.136:80/" -P
 IMPORTANT: Remember '/' at the end!

 H_RANGE variable can be changed, depending on how many pictures you
 want to be added with option H """

FILEPATH = "/home/ubuntu/trafficscript/sentences.txt" #change for environment
TEMPDIR = "/run/shm/"
LINE_LIMIT = 2200
SENTENCE_LENGTH = 150
H_RANGE = 50  # Variable can be changed depending on wanted load.


FOUND_OPTION_U = False
OPTIONS, REM = getopt.getopt(sys.argv[1:], 'U:PV:FC:HR')
for alt, argum in OPTIONS:  #Check if arguments have -U
    if alt in '-U':
        URL = argum
        FOUND_OPTION_U = True

    if not FOUND_OPTION_U:
        print "NO URL GIVEN"
        sys.exit()

def get_webpage_items():
    """get number of pictures from webpage"""

    rand = get_int(0, 1000)  # Get random int for folder name
    folder = TEMPDIR + str(rand) #make folder path of tempdir and random int

    os.mkdir(folder)  # Make new folder
    os.chdir(folder)  # Change directory to folder
    os.system("wget -t 2 -T 5 -q "+URL) # Wget frontpage

    tempfile = folder+"/index.html"
    if os.path.isfile(tempfile):  # Check if file exists
        index_file = open(tempfile) # Opens it
        var = index_file.read() # Read entire file
        var2 = var.split()  # split file into lines
        search_item = "database:" # What to search for
        search_index = var2.index(search_item)
          # find the index of the searched item.
        database_count = int(var2[search_index+1])
          # Set next item after search item to be the count,
          # as the following item will be a number
        index_file.close() #close file
        os.system("rm -r "+folder)  # Remove folder and its contents
        return database_count
    else:
        print "Error, could not find or locate database_count"

def get_int(arg1, arg2):
    """ returns random int in range of arg1 and arg2"""
    return random.randint(arg1, arg2)

def is_sentence(txt):
    """ Function for check if sentence starts with not allowed characters"""
    return txt.startswith('<<') or txt.startswith('>>') or \
               txt.startswith(',') or txt.startswith('ELECTRONIC') or \
               txt.startswith('PROHIBITED')

def check_sentence_length(text):
    """ Function to check if sentence is within limits """
    if len(text) > SENTENCE_LENGTH:  # Check if sentence is longer than allowed
        temp = text[:SENTENCE_LENGTH].split()
          # Split sentence into separate words, but only up to allowed length
        new_sentence = " ".join(temp[:len(temp)-1])
          # join the words from the list, -1 so last word isnt split
        return new_sentence    # return new sentence
    else:
        return text  # if not longer than allowed, return original


def front_page():
    """ Function, visit frontpage of flackr"""
    rand = get_int(0, 1000)  # Get random int for folder name

    folder = TEMPDIR + str(rand) # make folder path of tempdir and random int
    os.mkdir(folder)  # Make new folder
    os.chdir(folder)  # Change directory to folder

    os.system("wget -t 2 -T 5 -p -q "+URL)
      # Wget all elements from webpage inc css,logo,photos
    os.system("rm -r "+folder)  # Remove folder and its contents

    print "visited front page"

def get_random_name():
    """ Function for getting a random user name"""
                      # Downloads a json document with random user information
    response = urllib2.urlopen( \
      'http://api.randomuser.me/1.0/?nat=gb,us&inc=name&noinfo').read()
    parsed_json = json.loads(response)  # Parse the json into variable
    firstname = parsed_json["results"][0]["name"]["first"].title()
        # Select firstname and capitalize
    lastname = parsed_json["results"][0]["name"]["last"].title()
        # select Lastname and capitalize

    name = firstname + " " + lastname     # Add names together

    return name     # Return generated name

def get_random_title():
    """ Function for making a random title"""
            # Same procedure as previous function
    response = urllib2.urlopen( \
      'http://api.randomuser.me/1.0/?nat=gb,us&inc=name&noinfo').read()
    parsed_json = json.loads(response)
    temp = parsed_json["results"][0]["name"]["last"].title()
      # Select only lastname and capitalize
    title = "The" + " " + temp    # Make it sound like a title

    return title      # Return generated title

def new_picture():
    """ Function for adding new picture to website"""
    name = get_random_name()  # Get name and title
    title = get_random_title()
          # uses urllib module for opening up connection to webpage
          #  and add a new picture using the /insert route
    response = urllib.urlopen(URL+"insert/newUser?title=" + \
                              title + "&publisher=" + name \
                              + "&image=http://lorempixel.com/1280/720/")
    #print "%s" % response
    print "added new picture"

def gen_views(option):
    """ Has 2 functions,visit all once,visit random, visit 1,
    visit a few many times. The idea is the same as explained
    in option A for all of them,  only difference is
    how many pages are visited. """

    items = get_webpage_items()
    opt_random = False
    if option == 'R':
        item_range = items * 2  # Visit all pages or random
        opt_random = True
    else:
        item_range = items
    for item in range(1, item_range + 1):
     # Range from 1 to nr of pictures,compensate +1 for range
        rand = get_int(0, 1000) # Generate random int for foldername
        if opt_random:
            item_id = get_int(1, items)
        else:
            item_id = item
        urlsite = URL+str(item_id) # Make URL
        folder = TEMPDIR+str(rand) #Make folder path
        os.mkdir(folder)      # Make folder
        os.chdir(folder)      # Change directory to folder
        os.system("wget -t 2 -T 5 -p -q "+urlsite)
                    # Download everything from webpage
        os.system("rm -r "+folder)  # Delete again
        print "View "+str(item_id)
    print "Visited "+str(item_range) + " number of sites"

def get_random_text():
    """ Get a random sentence from the sentences file"""
    find_line = False  # bool variable

    if os.path.isfile(FILEPATH):  # check if file can be found
        temp = open(FILEPATH)     # Open file
        text = temp.readlines()   # read all the lines
        while find_line is False:  # Until it finds a proper line
            line = get_int(0, LINE_LIMIT)  # Get random line
            if is_sentence(text[line]) is False:
              # Use is_sentence function to check if acceptable
                find_line is True    # Set bool value to true to stop while loop
                sentence = check_sentence_length(text[line])
                # Check if sentence is too long
                return sentence   # Return the sentence
        temp.close()
    else:
        print "Could not find file!!!"

def gen_comment(option):
    """ 2 functions, comment to all once, comment to random x times.
    All options use the same method,
    only difference being amount of comments """

    items = get_webpage_items()
    opt_random = False
    if option == 'R':
        item_range = items / 2
        opt_random = True
    else:
        item_range = items
    for item in range(1, item_range + 1):
     # comment to all or random, +1 is compensation for range.
        name = get_random_name()    # Get random name from function
        text = get_random_text()    # Get a sentence from the sentences file
        if opt_random:     # If option R, get random int for ID
            item_id = get_int(1, items)
        else:
            item_id = item  # Open up the url /insert route
        response = urllib.urlopen(URL+"insert/newComment?id=" \
                                  + str(item_id) +"&name=" + name \
                                  + "&text=" + text)
        #print "%s" % response
        print "generated comment to: " + str(item_id)

def random_operation():
    """ Function for doing one of the provided options
      randomly """
    random_number = get_int(0, 100)  # Gets int in provided range
    if random_number <= 25: # Depending on int it does one of the options
        new_picture()
    elif random_number > 25 and random_number <= 50:
        gen_comment('R')
    elif random_number > 50 and random_number <= 75:
        gen_views('R')
    elif random_number > 75 and random_number <= 100:
        front_page()

def main():
    """ Main menu, takes option as command line argument"""
    for opt, arg in OPTIONS:  # And based on input, calls required functions
        if opt in '-P':
            new_picture()
        elif opt in '-F':
            front_page()
        elif opt in '-C':
            if arg == 'A' or arg == 'R':
                gen_comment(arg)
            else:
                raise ValueError("wrong ARGUMENT! A or R!!!")
        elif opt in '-V':
            if arg == 'A' or arg == 'R':
                gen_views(arg)
            else:
                raise ValueError("Wrong argument! A or R!!!")
        elif opt in '-H':  # For now it adds H_RANGE users
            for _ in range(H_RANGE):
                new_picture()

            front_page()  # Visits frontpage
            gen_comment('A')  # comments to all
            gen_views('A')    # views all
        elif opt in '-R':
            random_operation()

if __name__ == '__main__':
    main()
